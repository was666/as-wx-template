import http from '../utils/http.js'

export default {
  getList:params => {
    return http.post('/api/user/getList', params)
  }
}
